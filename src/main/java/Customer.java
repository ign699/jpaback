import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

@Entity
public class Customer extends Company {

   /* @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;*/

    private double discount;
    @OneToMany
    private List<Transactionn> transactionns = new LinkedList<Transactionn>();

    public Customer() {
    }

    public Customer(double discount, String name,  String street, String city, String zipCode) {
        super(name, street, city, zipCode);
        this.discount = discount;
    }

   /* @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }*/

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }
}