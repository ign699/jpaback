import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int ProductId;
    private String ProductName;
    private int UnitsOnStock;

    @ManyToOne
    @JoinColumn(name="SUPP_FK")
    private Supplier supplier;

    @ManyToMany
    private Set<Transactionn> transactions = new HashSet<Transactionn>();


    public Product() {

    }

    public Product(String ProductName, int UnitsInStock) {
        this.ProductName = ProductName;
        this.UnitsOnStock = UnitsInStock;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public void setUnitsOnStock(int unitsOnStock) {
        UnitsOnStock = unitsOnStock;
    }

    public String getProductName() {
        return ProductName;
    }

    public int getUnitsOnStock() {
        return UnitsOnStock;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public void addTransaction2Set(Transactionn transaction) {
        this.transactions.add(transaction);
    }
}
