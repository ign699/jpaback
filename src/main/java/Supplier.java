import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity

public class Supplier extends Company{
    /*@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int SupplierId;*/

    @OneToMany
    @JoinColumn(name="SUPP_FK")
    private Set<Product> products = new HashSet<Product>();


    private int AccountNumber;
    public Supplier() {

    }

    public Supplier(String companyName, String city, String street, String zipCode, int AccountNumber) {
        super(companyName, street, city, zipCode);
        this.AccountNumber = AccountNumber;
    }


    public void addProductToSet(Product product) {
        this.products.add(product);
        product.setSupplier(this);
    }
}
