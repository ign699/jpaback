import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.xml.internal.bind.v2.runtime.reflect.Lister;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static spark.Spark.*;



public class HibRunner {
    private static SessionFactory sessionFactory = null;
    public static void main(String[] args) {
        //initDB();
        Gson gson = new Gson();
        port(8080);
        options("/*",
                (request, response) -> {

                    String accessControlRequestHeaders = request
                            .headers("Access-Control-Request-Headers");
                    if (accessControlRequestHeaders != null) {
                        response.header("Access-Control-Allow-Headers",
                                accessControlRequestHeaders);
                    }

                    String accessControlRequestMethod = request
                            .headers("Access-Control-Request-Method");
                    if (accessControlRequestMethod != null) {
                        response.header("Access-Control-Allow-Methods",
                                accessControlRequestMethod);
                    }

                    return "OK";
                });

        before((request, response) -> response.header("Access-Control-Allow-Origin", "*"));

        get("/products", (req, res) -> {
            List products = getProducts();
            //products.forEach(product -> {System.out.println(((Product)product).getProductName());});
            //return "hellop";
            return gson.toJson(products);
        });

        get("/customers", (req, res) -> {
            List customers = getCustomers();
            return gson.toJson(customers);
        });

        post("/order", (req, res) -> {
            System.out.println(req.body());
            int[][] list = gson.fromJson(req.body(), int[][].class);
            EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("DB");
            EntityManager entityManager = entityManagerFactory.createEntityManager();
            EntityTransaction entityManagerTx = entityManager.getTransaction();
            entityManagerTx.begin();
            entityManager.find(Customer.class, list[0][0]);
            Transactionn transactionn = new Transactionn();

            for(int j = 1; j< list.length; j++){
                Product prod = entityManager.find(Product.class, list[j][0]);
                transactionn.addProduct(prod, list[j][1]);
            }
            entityManager.persist(transactionn);
            entityManagerTx.commit();
            entityManager.close();
            return 200;
        });






    }

    private static List getProducts(){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("DB");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction entityManagerTx = entityManager.getTransaction();
        entityManagerTx.begin();
        List products = entityManager.createQuery("select p.ProductId, p.ProductName, p.UnitsOnStock from Product p").getResultList();
        entityManagerTx.commit();
        entityManager.close();
        return products;
    }

    private static List getCustomers(){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("DB");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction entityManagerTx = entityManager.getTransaction();
        entityManagerTx.begin();
        List customers = entityManager.createQuery("select c.id, c.companyName from Customer c").getResultList();
        entityManagerTx.commit();
        entityManager.close();
        return customers;
    }

    private static void initDB() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("DB");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction entityManagerTx = entityManager.getTransaction();
        entityManagerTx.begin();


        Product product = new Product("p1", 2);
        Product product1 = new Product("p2", 2);
        Product product2 = new Product("p3", 2);
        Product product3 = new Product("p4", 2);
        Product product4 = new Product("p5", 2);

        entityManager.persist(product);
        entityManager.persist(product1);
        entityManager.persist(product2);
        entityManager.persist(product3);
        entityManager.persist(product4);


        Customer customer = new Customer(0.1, "c1", "c1","c1","c1");
        Customer customer1 = new Customer(0.2, "c2", "c2","c2","c2");
        Customer customer2 = new Customer(0.3, "c3", "c3","c3","c3");
        Customer customer3 = new Customer(0.4, "c4", "c4","c4","c4");

        entityManager.persist(customer);
        entityManager.persist(customer1);
        entityManager.persist(customer2);
        entityManager.persist(customer3);

        Supplier supplier = new Supplier("s1","s1","s1","s1",1);
        Supplier supplier1 = new Supplier("s2","s2","s3","s4",2);
        Supplier supplier2 = new Supplier("s3","s3","s3","s3",3);

        entityManager.persist(supplier);
        entityManager.persist(supplier1);
        entityManager.persist(supplier2);







        entityManagerTx.commit();
        entityManager.close();
    }
    private static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            Configuration configuration = new Configuration();
            sessionFactory =
                    configuration.configure().buildSessionFactory();
        }
        return sessionFactory;
    }
}